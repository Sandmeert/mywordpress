//Nameless function
(function() {
    $('.scrolltotop').click(function(ev) {
        ev.preventDefault();
        $('html, body').animate({scrollTop:0}, 600);
        return false;
    });

    $(window).scroll(function(ev) {
        if($(this).scrollTop() > 0)
        {
            $('.scrolltotop').fadeIn();
        } else  {
            $('.scrolltotop').fadeOut();
        }

    });
})();
