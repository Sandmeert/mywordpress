<?php

?>
<aside>
    <section>
        <?php if ( dynamic_sidebar('home_right_2') ) : else : endif; ?>
    </section>
    <section>
        <h3>Links</h3>
        <?php wp_nav_menu (array('theme_location' => 'tertiary','menu_class' => 'sidemenu'));?>
    </section>
    <section>
        <h3>Newsletter</h3>
        <?php if ( dynamic_sidebar('home_right_1') ) : else : endif; ?>
    </section>
</aside>
</main>
<div class="clearfix"></div>
</div>

<div id="footer_wrapper">
    <footer>
        <p>
            Design door Sander Meert<br>
            Gemaakt in opdracht van Arteveldehogeschool Gent
        </p>
    </footer>
</div>

<span class="scrolltotop"><i class="fa fa-arrow-up fa-2x"></i></span>

</div>
<!-- BOTTOM SCRIPTS -->
<script src="components/jquery/jquery-2.1.0.js"></script>
<script src="components/bootstrap/js/bootstrap.min.js"></script>
<!-- MY OWN SCRIPTS -->
<script src="scripts/app.js"></script>
<?php wp_footer(); ?>
</body>
</html>