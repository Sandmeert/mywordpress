<?php
add_action('init','register_my_menus');
function register_my_menus(){
    register_nav_menus( array(
        'primary'   => __( 'Top primary menu'),
        'secondary' => __( 'Secondary menu in left sidebar'),
        'tertiary' => __( 'Tertiary menu in left sidebar'),
    ) );
};
/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

    register_sidebar( array(
        'name' => 'Home right sidebar',
        'id' => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
    ) );
    register_sidebar( array(
        'name' => 'Recent posts',
        'id' => 'home_right_2',
        'before_widget' => '<div class="recent_posts">',
        'after_widget' => '</div>',
    ) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );
?>