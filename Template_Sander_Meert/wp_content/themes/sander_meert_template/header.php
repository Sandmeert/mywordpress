<?php

?><!DOCTYPE html>
<!-- [if lt IE 7]><html dir="ltr" lang="nl-be" prefix="og: http://ogp.me/ns#" class="no-js lt-ie7"><![endif]-->
<!-- [if lt IE 8]><html dir="ltr" lang="nl-be" prefix="og: http://ogp.me/ns#" class="no-js lt-ie8"><![endif]-->
<!-- [if lt IE 9]><html dir="ltr" lang="nl-be" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9"><![endif]-->
<!-- [if lt IE 10]><html dir="ltr" lang="nl-be" prefix="og: http://ogp.me/ns#" class="no-js lt-ie10"><![endif]-->
<!-- [if gt IE 9|!(IE)]><!--><html dir="ltr" lang="nl-be" prefix="og: http://ogp.me/ns#" class="no-js"><!--<![endif]-->
<head>
    <!-- CHARACTER ENCODING -->
    <meta charset="UTF-8">
    <title><?=bloginfo('name'); ?>  <?php wp_title(); ?></title>
    <!-- USE LATEST VERSION OF THE RENDERING ENGINE -->
    <meta http-equiv="X-UA-Compatible" content="IE-edge,chrome=1">
    <!-- MOST SEO HEADTAG -->

    <!-- OTHER IMPORTANT SEO TAGS -->
    <meta name="author" content="Arteveldehogeschool | Bachelor in de grafische en digitale media | Sander Meert">
    <meta name="copyright" content="2013-14 Copyright Arteveldehogeschool | All rights reserved">
    <meta name="viewport" content="width=device-width; initial-scale=1.0;">
    <!-- LINK NORMALIZE, GLOBAL, APP CSS -->
   <!-- <link rel="stylesheet" href="styles/normalize.css">

    <link rel="stylesheet" href="components/fontawesome/css/font-awesome.css">
    <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"
    <!-- MODERNIZR: NEW HTML ELEMENTS + FEATURE DETECTION OF CLIENT'S BROWSER -->
    <script src="<?php bloginfo('template_url'); ?>/components/modernizr/modernizr.custom.js"></script>
    <?php wp_head(); ?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/app.js"></script>
    <?php  define('WP_USE_THEMES', false); get_header(); ?>

</head>
<body>
<div id="page_wrapper">
    <div class="blog-masthead" role="navigation">
        <nav class="blog-nav">
            <!--
            <a class="blog-nav-item active" href="#">Home</a>
            <a class="blog-nav-item" href="#">Link</a>
            <a class="blog-nav-item" href="#">Link</a>
            <a class="blog-nav-item" href="#">Link</a>
            <a class="blog-nav-item" href="#">Link</a>
            -->

        <?php wp_nav_menu (array('theme_location' => 'primary','menu_class' => 'menu'));?>
        </nav>
    </div>
    <div id="header_wrapper">
        <header>
            <h1><a href="<?php echo home_url(); ?>" title="home"><?=bloginfo('name'); ?></a></h1>
        </header>
    </div>

    <div id="main_wrapper">
        <main>
