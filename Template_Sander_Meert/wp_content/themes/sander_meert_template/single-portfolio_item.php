<?php
/**
 * Created by PhpStorm.
 * User: WinMacSander
 * Date: 31/03/14
 * Time: 14:05
 * Template Name: New Template
 *
*/
get_header(); ?>
<section>
            <?php
            $mypost = array( 'post_type' => 'portfolio_items', );
            $loop = new WP_Query( $mypost );
            ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post();?>

                <article>
                    <h1><?=the_title(); ?></h1>
                    <small><?php the_author_posts_link() ?></small>
                        <?=the_content(); ?>
                        <br>
                        <b>Vak:</b>
                        <?php echo esc_html( get_post_meta( get_the_ID(), 'portfolio_vak_name', true ) ); ?>

                        <br><b>Preview: </b> <small>(klik voor grotere afbeelding)</small>
                        <br>
                        <?php
                        echo '<a href="' . get_post_meta( get_the_ID(), 'portfolio_afbeelding', true ) .'"><img src="' . get_post_meta( get_the_ID(), 'portfolio_afbeelding', true ) . '" class="portfolio-image" /></a>';
                        ?>

                    <p>
                        <?php echo dvk_social_sharing(); ?>
                    </p>
                </article>
                <hr>
            <?php endwhile; ?>
<?php wp_reset_query(); ?>
</section>
<?php get_footer(); ?>