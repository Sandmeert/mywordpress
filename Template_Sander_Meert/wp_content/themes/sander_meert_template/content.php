<?php
/**
 * Created by PhpStorm.
 * User: WinMacSander
 * Date: 23/05/14
 * Time: 10:08
 */
?>
<section>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article>
        <h1><a href=" <?php the_permalink(); ?> "><?=the_title(); ?></a></h1>
        <small><?php the_time('F jS, Y') ?> by <?php the_author_posts_link() ?></small>
        <br><?php echo get_avatar(get_the_author_meta('ID'), 64); ?>

        <?=the_content(); ?>
        <div class="clearfix"</div>

    </article>
                <hr>
<?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
</section>