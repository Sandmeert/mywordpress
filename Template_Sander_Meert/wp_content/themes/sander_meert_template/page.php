<?php
get_header(); ?>
<section>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article>
        <h1><?=the_title(); ?></h1>

        <?=the_content(); ?>
        <p>
            <?php echo dvk_social_sharing(); ?>
        </p>

    </article>
<?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
</section>
<?php get_footer(); ?>