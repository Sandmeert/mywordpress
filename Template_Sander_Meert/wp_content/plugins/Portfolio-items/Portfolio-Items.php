<?php
/*
 * Plugin Name: Portfolio Items
 * Plugin URI: http://localhost:81/wordpress/
 * Description: Declares a plugin that will make a custom post type displaying portfolio items.
 * Version: 0.1
 * Author: Sander Meert
 * Authoer URI: http://www.arteveldehs.be/
 * Licence: Open
 */

add_action( 'init', 'create_portfolio_item' );
function create_portfolio_item() {
    register_post_type( 'portfolio_items',
        array(
            'labels' => array(
                'name' => 'Portfolio Items',
                'singular_name' => 'Portfolio Item',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Portfolio Item',
                'edit' => 'Edit',
                'edit_item' => 'Edit Portfolio Item',
                'new_item' => 'New Portfolio Item',
                'view' => 'View',
                'view_item' => 'View Portfolio Item',
                'search_items' => 'Search Portfolio Items',
                'not_found' => 'No Portfolio Items found',
                'not_found_in_trash' => 'No Portfolio Items found in Trash',
                'parent' => 'Parent Portfolio Item'
            ),

            'public' => true,
            'menu_position' => 16,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail' ),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}

add_action('admin_init','my_portfolio_admin');
function my_portfolio_admin() {
    add_meta_box('portfolio_item_meta_box',
    'Portfolio Item Details',
    'display_portfolio_item_meta_box',
    'portfolio_items','normal','high');
}

function display_portfolio_item_meta_box( $portfolio_item ) {

    $portfolio_vak = esc_html( get_post_meta( $portfolio_item->ID, 'portfolio_vak_name', true ) );
    $portfolio_afbeelding = esc_html( get_post_meta( $portfolio_item->ID, 'portfolio_afbeelding', true ) );
    ?>

    <table>
        <tr>
            <td style="width: 100%">Portfolio Vak</td>
            <td><input type="text" size="80" name="portfolio_review_vak_name" value="<?php echo $portfolio_vak; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Portfolio Afbeelding Link</td>
            <td><input type="text" size="80" name="portfolio_review_afbeelding_link" value="<?php echo $portfolio_afbeelding; ?>" /></td>
        </tr>
    </table>
<?php }

add_action( 'save_post', 'add_portfolio_item_fields', 10, 2 );

function add_portfolio_item_fields($portfolio_item_id, $portfolio_item ) {
// Check post type for portfolio items
    if ( $portfolio_item->post_type == 'portfolio_items' ) {
// Store data in post meta table if present in post data
        if ( isset( $_POST['portfolio_review_vak_name'] ) && $_POST['portfolio_review_vak_name'] != '' ) {
            update_post_meta( $portfolio_item_id, 'portfolio_vak_name', $_POST['portfolio_review_vak_name'] );
        }
        if ( isset( $_POST['portfolio_review_afbeelding_link'] ) && $_POST['portfolio_review_afbeelding_link'] != '' ) {
            update_post_meta( $portfolio_item_id, 'portfolio_afbeelding', $_POST['portfolio_review_afbeelding_link'] );
        }
    }
}

add_filter( 'template_include', 'include_portfolio_template_function', 1 );
function include_portfolio_template_function( $template_path ) {
    if ( get_post_type() == 'portfolio_items' ) {
        if ( is_single() ) { // checks if the file exists in the theme first,
        // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-portfolio_item.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/single-portfolio_item.php';
            }
        }
    }
    return $template_path;
}

?>